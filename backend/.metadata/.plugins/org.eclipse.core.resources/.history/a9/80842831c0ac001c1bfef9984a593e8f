package com.shop.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.criteria.Fetch;
import javax.validation.constraints.Email;
import javax.validation.constraints.Pattern;
@Entity
@Table(name = "user")
public class User {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int userId;         
	private String userFirstname;  
	private String userLastname;
	@Pattern(regexp="^[0-9]{10}$")
	private String userPhone; 
	@Email
	private String userEmail;      
	private String userPassword;
	private String userAddress;     
	private String userPostalcode;  
	private int userStatus;     
	private String userRole;
	
	
	@OneToMany(cascade = CascadeType.ALL,fetch = FetchType.EAGER)
	@JoinColumn(name="userId", referencedColumnName = "userId")
	private List<MyOrder> myorderList;
	

	@OneToMany(cascade = CascadeType.ALL,fetch = FetchType.EAGER)
	@JoinColumn(name="vendorId", referencedColumnName = "userId")
	private List<Product> productList;
	
	
	@OneToMany(cascade = CascadeType.ALL,fetch = FetchType.EAGER)
	@JoinColumn(name="userId", referencedColumnName = "userId")
	private List<Cart> cartList;
	
	@OneToMany(cascade = CascadeType.ALL,fetch = FetchType.EAGER)
	@JoinColumn(name="userId", referencedColumnName = "userId")
	private List<Payment> paymentList;
	
	@OneToMany(cascade = CascadeType.ALL,fetch = FetchType.EAGER)
	@JoinColumn(name="userId", referencedColumnName = "userId")
	private List<CardDetail> cardDetailsList;

public User() {
	// TODO Auto-generated constructor stub
}
	
public User(int userId, String userFirstname, String userLastname, @Pattern(regexp = "^[0-9]{10}$") String userPhone,
		@Email String userEmail, String userPassword, String userAddress, String userPostalcode, int userStatus,
		String userRole, List<Product> productList) {
	super();
	this.userId = userId;
	this.userFirstname = userFirstname;
	this.userLastname = userLastname;
	this.userPhone = userPhone;
	this.userEmail = userEmail;
	this.userPassword = userPassword;
	this.userAddress = userAddress;
	this.userPostalcode = userPostalcode;
	this.userStatus = userStatus;
	this.userRole = userRole;
	this.productList = productList;
}

public int getUserId() {
	return userId;
}

public void setUserId(int userId) {
	this.userId = userId;
}

public String getUserFirstname() {
	return userFirstname;
}

public void setUserFirstname(String userFirstname) {
	this.userFirstname = userFirstname;
}

public String getUserLastname() {
	return userLastname;
}

public void setUserLastname(String userLastname) {
	this.userLastname = userLastname;
}

public String getUserPhone() {
	return userPhone;
}

public void setUserPhone(String userPhone) {
	this.userPhone = userPhone;
}

public String getUserEmail() {
	return userEmail;
}

public void setUserEmail(String userEmail) {
	this.userEmail = userEmail;
}

public String getUserPassword() {
	return userPassword;
}

public void setUserPassword(String userPassword) {
	this.userPassword = userPassword;
}

public String getUserAddress() {
	return userAddress;
}

public void setUserAddress(String userAddress) {
	this.userAddress = userAddress;
}

public String getUserPostalcode() {
	return userPostalcode;
}

public void setUserPostalcode(String userPostalcode) {
	this.userPostalcode = userPostalcode;
}

public int getUserStatus() {
	return userStatus;
}

public void setUserStatus(int userStatus) {
	this.userStatus = userStatus;
}

public String getUserRole() {
	return userRole;
}

public void setUserRole(String userRole) {
	this.userRole = userRole;
}

public List<Product> getProductList() {
	return productList;
}

public void setProductList(List<Product> productList) {
	this.productList = productList;
}

@Override
public String toString() {
	return "User [userId=" + userId + ", userFirstname=" + userFirstname + ", userLastname=" + userLastname
			+ ", userPhone=" + userPhone + ", userEmail=" + userEmail + ", userPassword=" + userPassword
			+ ", userAddress=" + userAddress + ", userPostalcode=" + userPostalcode + ", userStatus=" + userStatus
			+ ", userRole=" + userRole + ", productList=" + productList + "]";
}



	
 
}
