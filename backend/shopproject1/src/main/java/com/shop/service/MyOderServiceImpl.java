package com.shop.service;

import com.shop.DTOConvert.MyOrderDTOConverter;
import com.shop.dao.MyOrderDAO;
import com.shop.dao.OrderDetailsDao;
import com.shop.dao.UserDao;
import com.shop.dto.MyOrderDTO;
import com.shop.dto.OrderDetailsCommentDTO;
import com.shop.entity.MyOrder;
import com.shop.entity.OrderDetail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Transactional
@Service
public class MyOderServiceImpl {
    @Autowired
    private MyOrderDTOConverter myOrderDTOConverter;
    @Autowired
    UserDao userDao;
    @Autowired
    MyOrderDAO myOrderDAO;

    @Autowired
    private OrderDetailsDao orderDetailsDao;

    public List<MyOrderDTO> getMyOrders(int id) {
        //List<MyOrder> list = myOrderDAO.findByUserId(id);

        List<MyOrder>list = userDao.findByUserId(id).getMyorderList();
        list.forEach(System.out::println);
        List<MyOrderDTO> myOrderDTOList = list.stream()
                .filter(p-> p.getStatus()!=2  )
                .map(p->myOrderDTOConverter.toMyOrderDTO(p)).collect(Collectors.toList());
        myOrderDTOList.forEach(System.out::println);
        return myOrderDTOList;

    }


    public void updateMyOrder(MyOrderDTO myOrderDTO) {
         MyOrder myOrder = myOrderDAO.findByMyorderId(myOrderDTO.getMyorder_id());
         myOrder.setStatus(Integer.parseInt(myOrderDTO.getStatus()));
         myOrderDAO.save(myOrder);
    }

//    public List<OrderDetailsCommentDTO> findAll() {
//         List<OrderDetail> list=  orderDetailsDao.findAll();
//         if(list != null)
//         {
//
//         }


}
