package com.shop.service;

import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import com.shop.dao.ProductDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shop.DTOConvert.CompanyDTOConverter;
import com.shop.dao.CompanyDAO;
import com.shop.dto.CompanyDTO;
import com.shop.entity.Company;
@Transactional
@Service
public class CompanyServiceImpl {

	@Autowired
	private CompanyDAO companyDAO;
	
	@Autowired
	private CompanyDTOConverter companyDTOConverter;

	@Autowired
	ProductDao productDao;
	
	
	public List<CompanyDTO> getAllCompany()
	{
		return companyDAO.findAll()
				.stream()
				.map(p->companyDTOConverter.toCompanyDTO(p))
				.collect(Collectors.toList());
					}
	
	public Company saveCompany(CompanyDTO companyDTO)
	{
		Company company = companyDTOConverter.toEntityCompanyDTO(companyDTO);
		return companyDAO.save(company);
	}

	public void deleteCompany(int id)
	{
//		productDao.deleteAllByCompId(id);
		companyDAO.deleteByCompId(id);
	}

	public Company updateCompany (CompanyDTO companyDTO)
	{
		Company company= companyDAO.findByCompId(companyDTO.getComp_id());
		company.setCompTitle(companyDTO.getComp_title());
		company.setCompDescription(companyDTO.getComp_description());
		return companyDAO.save(company);
	}
	
}
