package com.shop.service;

import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shop.DTOConvert.AddressDTOConverter;
import com.shop.dao.AddressDAO;
import com.shop.dto.AddressDTO;
import com.shop.entity.Address;

@Transactional
@Service
public class AddressServiceImpl 
{
	@Autowired
	private AddressDAO addressDao;
	
	@Autowired
	private AddressDTOConverter addressDTOConverter;
	
	
	public List<AddressDTO> findAllAddressById(int id)
	{
		List<AddressDTO> address =  addressDao.findAllByUserId(id)
				.stream()
				.map(p -> addressDTOConverter.toAddressDTO(p))
				.collect(Collectors.toList());
		return address;
	}
	
	public Address saveAddress(int id, AddressDTO addressDTO)
	{
		return addressDao.save(addressDTOConverter.toAddressEntity(id,addressDTO));
	}
	
	
	public List<AddressDTO> findAllAddressByAddId(int id)
	{
		List<AddressDTO> address =  addressDao.findAllByAddId(id)
				.stream()
				.map(p -> addressDTOConverter.toAddressDTO(p))
				.collect(Collectors.toList());
		return address;
	}
	
	public Address updateAddress(int id, AddressDTO addressDTO)
	{
		Address address = addressDao.findByUserId(id);
		address.setAddress(addressDTO.getAddress());
		address.setCity(addressDTO.getCity());
		address.setState(addressDTO.getState());
		address.setCountry(addressDTO.getCountry());
		address.setPin(addressDTO.getPin());
		return addressDao.save(address);

	}
	
	public void deleteAddress(int id)
	{
		addressDao.deleteById(id);
	}
	
	
	
}
