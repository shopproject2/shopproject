package com.shop.service;

import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shop.DTOConvert.CartDTOConverter;
import com.shop.dao.CartDAO;
import com.shop.dto.CartDTO;
import com.shop.entity.Cart;

@Transactional
@Service
public class CartServiceImpl 
{
	
	@Autowired
	private CartDTOConverter cartDTOConverter;
	
	@Autowired
	private CartDAO cartDao;
	
	
	
	public List<CartDTO> findCartItemsByUserId(int id)
	{
		return cartDao.findByUserId(id)
				.stream()
				.map(p -> cartDTOConverter.toCartDTO(p))
				.collect(Collectors.toList());
	}
	
	public Cart updateCart(int userId, CartDTO cartDTO)
	{
		//Cart cart = cartDTOConverter.toEntityCartUpdate(userId,cartDTO);
		//return cartDao.updateCart(cart.getCartQuantity(), cart.getCartId());
		//cartDao.findByCartId()

		Cart cart = cartDao.findByCartId(cartDTO.getCart_id());
		cart.setCartQuantity(cartDTO.getCart_quantity());
	 return cartDao.save(cart);
		
	}
	
	public Cart addToCart(int userId, CartDTO cartDTO)
	{

		Cart cart = cartDTOConverter.toEntityCart(userId,cartDTO);
		return cartDao.save(cart);
		
	}

	public void deleteCart(int cart_id) 
	{
		cartDao.deleteById(cart_id);
	}
	
	
}
