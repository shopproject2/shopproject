package com.shop.service;

import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import com.shop.dto.CompanyDTO;
import com.shop.entity.Company;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shop.DTOConvert.CategoryDTOConverter;
import com.shop.dao.CategoryDAO;
import com.shop.dto.CategoryDTO;
import com.shop.dto.ProductDTO;
import com.shop.entity.Category;

@Transactional
@Service
public class CategoryServiceImpl {
	
	@Autowired 
	private CategoryDAO categoryDAO;
	
	@Autowired
	private CategoryDTOConverter categoryDTOConverter;
	
	
	public List<CategoryDTO> getAllCategoryList()
	{
		return categoryDAO.findAll()
				.stream()
				.map(p->categoryDTOConverter.toCategoryDTO(p))
				.collect(Collectors.toList());
	}


    public Category saveCategory(CategoryDTO categoryDTO) {
		Category category =categoryDTOConverter.toEntityCategoryDTO(categoryDTO);
		return categoryDAO.save(category);
    }


	public Category updateCategory(CategoryDTO categoryDTO) {
		Category category = categoryDAO.findByCatId(categoryDTO.getCat_id());
		category.setCatTitle(categoryDTO.getCat_title());
		category.setCatDescription(categoryDTO.getCat_description());
	    return categoryDAO.save(category);

	}

	public void deleteCategory(int catId) {
		categoryDAO.deleteByCatId(catId);
	}
}
