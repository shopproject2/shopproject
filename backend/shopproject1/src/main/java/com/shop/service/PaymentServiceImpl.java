package com.shop.service;

import com.shop.DTOConvert.PaymentDTOConverter;
import com.shop.dao.PaymentDAO;
import com.shop.dto.PaymentDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Transactional
@Service
public class PaymentServiceImpl {
    @Autowired
    private PaymentDAO paymentDAO;
    @Autowired
    private PaymentDTOConverter paymentDTOConverter;

    public List<PaymentDto> findAllPayment() {

     return paymentDAO.findAll().stream().map(p->paymentDTOConverter.toPaymentDTO(p)).collect(Collectors.toList());
    }

    public Double totalPayment(){
    return paymentDAO.totalPayment();

    }
}
