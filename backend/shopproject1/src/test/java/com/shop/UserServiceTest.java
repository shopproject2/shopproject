package com.shop;

import java.util.Arrays;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.shop.dto.OrderDetailsCommentDTO;
import com.shop.entity.Cart;
import com.shop.entity.Product;
import com.shop.entity.User;
import com.shop.service.ProductServiceImpl;
import com.shop.service.UserServiceImpl;
@SpringBootTest
public class UserServiceTest {
	@Autowired
	private UserServiceImpl userServiceImpl;
	@Autowired
	private ProductServiceImpl productServiceImpl;
	@Autowired
	private OrderDetailsCommentDTO order;
	@Autowired
	private EntityManager entity;
	
	@Test
	@Transactional
	public void test()
	{
//		Object [] a =productServiceImpl.findCommentsProductById(1);
//		 OrderDetailsCommentDTO obj = (OrderDetailsCommentDTO)a[0];
//		System.out.println(obj);
		
		//List l =Arrays.asList(a);
		//l.stream().map(p-> order.setComment(p.))
	//	l.forEach(System.out::println);
		
//		TypedQuery<OrderDetailsCommentDTO> query
//	      = entity.createQuery(
//	          "SELECT\r\n"
//	          + "		user.user_name, \r\n"
//	          + "		orderdetails.rating, \r\n"
//	          + "		orderdetails.comment, \r\n"
//	          + "		orderdetails.product_id,\r\n"
//	          + "		myorder.order_date\r\n"
//	          + "	FROM\r\n"
//	          + "		user\r\n"
//	          + "		INNER JOIN\r\n"
//	          + "		myorder\r\n"
//	          + "		\r\n"
//	          + "			user.user_id = myorder.user_id\r\n"
//	          + "		INNER JOIN\r\n"
//	          + "		orderdetails\r\n"
//	          + "		 \r\n"
//	          + "			myorder.myorder_id = orderdetails.myorder_id\r\n"
//	          + "	WHERE\r\n"
//	          + "		product_id = 1", OrderDetailsCommentDTO.class);
//	    List<OrderDetailsCommentDTO> resultList = query.getResultList();
			
		productServiceImpl.findCommentsProductById(1).forEach(System.out::println);
		
	}
	
}
